//
//  RFSwipingPercentDrivenInteractiveTransition.swift
//  RFSwipingAnimatedTransitioning
//
//  Created by Nikita Arutyunov on 21.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit

public class RFSwipingPercentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition {
    public var hasStarted = false
    public var shouldFinish = false
}
