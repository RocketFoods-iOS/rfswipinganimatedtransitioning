//
//  RFSwipingAnimatedTransitioning.swift
//  RFSwipingAnimatedTransitioning
//
//  Created by Nikita Arutyunov on 21.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit

public class RFSwipingAnimatedTransitioning: NSObject {
    public weak var interactiveTransition: RFSwipingPercentDrivenInteractiveTransition?
    
    public let percentThreshold: CGFloat
    public let transitionDuration: TimeInterval
    
    public init(percentThreshold: CGFloat = 0.3, transitionDuration: TimeInterval = 0.6) {
        self.percentThreshold = percentThreshold
        self.transitionDuration = transitionDuration
    }
}

extension RFSwipingAnimatedTransitioning: UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else { return }
        
//        let containerView = transitionContext.containerView
        
//        containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        let screenBounds = UIScreen.main.bounds
        let bottomLeftCorner = CGPoint(x: 0, y: screenBounds.height)
        let finalFrame = CGRect(origin: bottomLeftCorner, size: screenBounds.size)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromVC.view.frame = finalFrame
            fromVC.view.alpha = 0
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

extension RFSwipingAnimatedTransitioning: UIViewControllerTransitioningDelegate {
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let interactiveTransitioning = self.interactiveTransition,
            interactiveTransitioning.hasStarted else { return nil }
        
        return interactiveTransitioning
    }
}
